const fizzbuzz = require('./fizzbuzz');

test('input can mod 15 should be count fizzbuzz', ()=> {
    expect(fizzbuzz(15)).toBe('fizzbuzz')
    expect(fizzbuzz(30)).toBe('fizzbuzz')
    expect(fizzbuzz(45)).toBe('fizzbuzz')
    expect(fizzbuzz(90)).toBe('fizzbuzz')
})

test('input can mod 3 should be count fizz', () => { 
    expect(fizzbuzz(3)).toBe('fizz')
    expect(fizzbuzz(6)).toBe('fizz')
    expect(fizzbuzz(9)).toBe('fizz')
    expect(fizzbuzz(99)).toBe('fizz')
})

test('input can mod 5 should be count buzz', ()=>{
    expect(fizzbuzz(5)).toBe('buzz')
    expect(fizzbuzz(10)).toBe('buzz')
    expect(fizzbuzz(100)).toBe('buzz')
})


test('testing Edge case', ()=> {
    expect(fizzbuzz(1)).toBe('1')
    expect(fizzbuzz(2)).toBe('2')
    expect(fizzbuzz(4)).toBe('4')
    expect(fizzbuzz(7)).toBe('7')
    expect(fizzbuzz(98)).toBe('98')
})
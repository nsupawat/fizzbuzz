module.exports = equation
const addnumber = require('./addnumber')
const subnumber = require('./subnumber')

function equation(a, b, c){
    let result = subnumber(a, b)
    if (result >= 0)
        return addnumber(result, c)
    return result
}
const equation = require('./equation')

jest.mock('./addnumber')
jest.mock('./subnumber')

test('testmockaddnumber2', () => {
    addnumber = require('./addnumber')
    subnumber = require('./subnumber')
    addnumber.mockReturnValue(100)
    subnumber.mockReturnValue(-1)
    // console.log(addnumber(1,1))
    expect(equation(1,1,1)).toBe(-1)
    expect(addnumber).not.toHaveBeenCalled()
    expect(subnumber).toHaveBeenCalledTimes(1)
    subnumber.mockReset()
})

test('testmockaddnumber', () => {
    addnumber = require('./addnumber')
    subnumber = require('./subnumber')
    addnumber.mockReturnValue(100)
    subnumber.mockReturnValue(10)
    // console.log(addnumber(1,1))
    expect(equation(1,1,1)).toBe(100)
    expect(addnumber).toHaveBeenCalledTimes(1)
    expect(subnumber).toHaveBeenCalledTimes(1)
    
})

